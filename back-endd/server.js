const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

//Initializing server
const app = express();
app.use(bodyParser());
const port = 8080;
app.listen(port, () => {
  console.log("Server online on: " + port);
});
app.use("/", express.static("../front-endd"));
const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database:"inscrisi"
});
connection.connect(function (err) {
  console.log("Connected to database!");
  const sql =
    "CREATE TABLE IF NOT EXISTS inscrisi(nume VARCHAR(255), prenume VARCHAR(255), universitate VARCHAR(50),facultate VARCHAR(250), an_studiu INTEGER, telefon VARCHAR(255), mail VARCHAR(255), domeniul VARCHAR(25), activitate1 VARCHAR(225), activitate2 VARCHAR(225),activitate3 VARCHAR(225))";
  connection.query(sql, function (err, result) {
    if (err) {
      throw err
    };
  });
});
app.post("/inscriere", (req, res) => {
  let nume = req.body.nume;
  let prenume = req.body.prenume;
  let universitate=req.body.universitate;
  let facultate=req.body.facultate;
  let an_studiu=parseInt(req.body.an_studiu);
  let telefon = req.body.telefon;
  let mail = req.body.mail;
  let domeniul = req.body.domeniul;
  let activitate1=req.body.activitate1;
  let activitate2=req.body.activitate2;
  let activitate3=req.body.activitate3;
  let GDPR=req.body.GDPR;
  let error = [];
  console.log(req.body);
  //validare: camp obligatoriu
  if (!nume||!prenume||!telefon||!mail||!universitate||!facultate||!domeniul ||!activitate1  || !activitate2 || !activitate3 || !an_studiu ) 
   { error.push("Unul sau mai multe campuri nu au fost introduse!");
     console.log("Unul sau mai multe campuri nu au fost introduse!");
   } else 
   {
      //validare: dimensiunea numelui sa fie intre 2 si 30 caractere
    if (nume.length < 2 || nume.length > 30) 
      {console.log("Nume invalid!");
       error.push("Nume invalid");
        }
      //validare: numele sa contina doar litere
     else if (!nume.match("^[A-Za-z]+$")) 
      {console.log("Numele trebuie sa contina doar litere!");
       error.push("Numele trebuie sa contina doar litere!");
      }
      //validare: dimensiunea prenumelui sa fie intre 2 si 30 caractere
    if (!prenume.length > 2 && !prenume.length < 30) 
     { console.log("Prenume invalid!");
       error.push("Prenume invalid!");
    } 
    //validare: prenumele sa contina doar litere
      else if (!prenume.match("^[A-Za-z]+$")) 
     { console.log("Prenumele trebuie sa contina doar litere!");
       error.push("Prenumele trebuie sa contina doar litere!");
    }
    //validare: universitatea sa aiba lungime de minim 2, maxim 50 
    if (!universitate.length > 2 && !universitate.length < 50) 
     { console.log("Universitatea a fost introdusa gresit!");
       error.push("Universitatea a fost introdusa gresit!");
    } 
    //validare: universitatea sa contina doar litere
      else if (!universitate.match("^[A-Za-z]+$")) 
     { console.log("Universitatea trebuie sa contina doar litere!");
       error.push("Universitatea trebuie sa contina doar litere!");
    }
    //validare: facultatea sa aiba lungime de minim 2, maxim 255  
    if (!facultate.length > 2 && !facultate.length < 255) 
     { console.log("Facultatea a fost introdusa gresit!");
       error.push("Facultatea a fost introdusa gresit!");
    } 
    //validare: facultatea sa contina doar litere
      else if (!facultate.match("^[A-Za-z]+$")) 
     { console.log("Facultatea trebuie sa contina doar litere!");
       error.push("Facultatea trebuie sa contina doar litere!");
    }
    //validare: anul de studiu trebuie sa contina 1 caracter
    if (an_studiu > 3) 
      {console.log("Anul de studiu nu a fost introdus corect");
       error.push("Anul de studiu nu a fost introdus corect");
    } 
    //validare: anul de studiu trebuie sa fie format doar din cifre
    
    //validare: nr telefon trebuie sa contina 10 caractere
    if (telefon.length != 10) 
      {console.log("Numarul de telefon trebuie sa fie de 10 cifre!");
       error.push("Numarul de telefon trebuie sa fie de 10 cifre!");
    } 
    //validare: nr telefon trebuie sa fie format doar din cifre
    else if (!telefon.match("^[0-9]+$")) 
     { console.log("Numarul de telefon trebuie sa contina doar cifre!");
       error.push("Numarul de telefon trebuie sa contina doar cifre!");
    }
    //vaidare: adresa sa fie de tip email sau gmail 
    if (!mail.includes("@gmail.com") && !mail.includes("@yahoo.com")) 
      {console.log("Email invalid!");
       error.push("Email invalid!");
    }
    

   
  }
  if (error.length === 0) {
    const sql =
      "INSERT INTO inscrisi (nume, prenume, universitate, facultate, an_studiu, telefon, mail, domeniul, activitate1, activitate2, activitate3,GDPR) VALUES ('" + nume + "','" + prenume + "','" + universitate + "','" + facultate + "','" + an_studiu + "','" + telefon + "','" + mail + "','" + domeniul + "','" + activitate1 + "','" + activitate2 + "','" + activitate3 + "', '" + GDPR + "')";
    connection.query(sql, function (err, result) {
      if (err) throw err;
      console.log("Inscrierea a fost efectuata cu succes!");
      res.status(200).send({
        message: "Inscrierea a fost efectuata cu succes!"
      });
      console.log(sql);
    });
  } else {
    res.status(500).send(error);
    console.log("Eroare la adaugarea in baze de date!");
  }
});